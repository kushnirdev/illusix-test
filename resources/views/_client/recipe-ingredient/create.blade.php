@extends('layouts.app')

@section('content')
    @php
        /** @var \App\Models\RecipeIngredient $item */
    @endphp
    <div class="container">

        @include('_client.recipe-ingredient.includes.result_messages')
        <form method="POST" action="{{ route('client.recipe-ingredients.store') }}">

            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Add ingredient</div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="recipe_id" hidden></label>
                                        <input name="recipe_id"
                                               value="{{ $id }}"
                                               id="recipe_id"
                                               type="number"
                                               class="form-control"
                                               hidden>
                                    </div>
                                    <div class="form-group">
                                        <label for="ingredient_id">Ingredient</label>
                                        <select name="ingredient_id"
                                                id="ingredient_id"
                                                type="text"
                                                class="form-control"
                                                placeholder="Choose ingredient"
                                                required>
                                            @foreach($ingredientList as $ingredientOption)
                                                <option value="{{ $ingredientOption->id }}">
                                                    {{ $ingredientOption->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="weight">Weight</label>
                                        <input name="weight"
                                               value="{{ old('weight', $item->weight) }}"
                                               id="weight"
                                               type="number"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
