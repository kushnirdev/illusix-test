@extends('layouts.app')
@section('content')
    <div class="container">
        @include('_client.recipe.includes.result_messages')
        <div class="row justify-content-center">

            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-primary ml-auto" href="{{ route('client.ingredients.create') }}">Add Ingredient</a>
                </nav>
                <div class="card">
                    <div class="card-header">Ingredients</div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                @php /** @var \App\Models\Ingredient $item */ @endphp
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ route('client.ingredients.edit', $item->id) }}">{{ $item->name }}</a></td>
                                    <td>{{ $item->description }}</td>
                                    <td>
                                        <form method="post" action="{{ route('client.ingredients.destroy', $item->id) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-link"><img src="/images/icon-trash.png" alt="Delete"  height="15" width="15"></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if($items->total() > $items->count())
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                {{ $items->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
