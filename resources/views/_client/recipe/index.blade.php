@extends('layouts.app')
@section('content')
    <div class="container">
        @include('_client.recipe.includes.result_messages')
        <div class="row justify-content-center">

            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-primary ml-auto" href="{{ route('client.recipes.create') }}">Add Recipe</a>
                </nav>
                <div class="card">
                    <div class="card-header">My Recipes</div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Recipe</th>
                                <th><pre>Description</pre></th>
                                <th>Ingredients</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                @php /** @var \App\Models\Recipe $item */ @endphp
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ route('client.recipes.show', $item->id) }}">{{ $item->name }}</a></td>
                                    <td><pre>{{ $item->description }}</pre></td>
                                    <td class="img-thumbnail bg-info ml-auto">{{ $item->recipe_ingredients_count }}</td>
                                    <td class="text-right">
                                        <form class="d-inline" method="get" action="{{ route('client.recipes.edit', $item->id) }}">
                                            <button type="submit" class="btn btn-link"><img src="/images/icon-edit.png" alt="Delete"  height="15" width="15"></button>
                                        </form>
                                        <form class="d-inline" method="post" action="{{ route('client.recipes.destroy', $item->id) }}">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit" class="btn btn-link"><img src="/images/icon-trash.png" alt="Delete"  height="15" width="15"></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if($items->total() > $items->count())
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                {{ $items->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
