<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Ingredients</div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Ingredient</th>
                        <th>Weight</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recipeIngredients as $recipeIngredient)
                    @php /** @var \App\Models\RecipeIngredient $recipeIngredient */ @endphp
                    <tr>
                        <td>{{ $recipeIngredient->id }}</td>
                        <td><a href="{{ route('client.recipe-ingredients.edit', $recipeIngredient->id) }}">{{ $recipeIngredient->ingredients->name }}</a></td>
                        <td>{{ $recipeIngredient->weight }}</td>
                        <td>
                            <form method="post" action="{{ route('client.recipe-ingredients.destroy', $recipeIngredient->id) }}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-link"><img src="/images/icon-trash.png" alt="Delete"  height="15" width="15"></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <a class="btn btn-primary" href="{{ route('client.recipe-ingredient.addingredient', $item->id) }}">Add Ingredient</a>
                <a class="btn btn-primary" href="{{ route('client.ingredients.create') }}">Create Ingredient</a>
            </div>
        </div>
    </div>
</div>

@if($recipeIngredients->total() > $recipeIngredients->count())
    <br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{ $recipeIngredients->links() }}
                </div>
            </div>
        </div>
    </div>
@endif
