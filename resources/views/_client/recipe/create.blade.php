@extends('layouts.app')

@section('content')
    @php
        /** @var \App\Models\Recipe $item */
    @endphp
    <div class="container">

        @include('_client.recipe.includes.result_messages')
        <form method="POST" action="{{ route('client.recipes.store') }}">

            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Create new recipe</div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="user_id" hidden></label>
                                        <input name="user_id"
                                               value="{{ $userId }}"
                                               id="user_id"
                                               type="number"
                                               class="form-control"
                                               hidden>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input name="name"
                                               value="{{ old('name', $item->name) }}"
                                               id="name"
                                               type="text"
                                               class="form-control"
                                               minlength="3"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea name="description"
                                                  rows="10"
                                                  id="description"
                                                  class="form-control">{{ old('description', $item->description) }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipe">Recipe</label>
                                        <textarea name="recipe"
                                                  rows="20"
                                                  id="recipe"
                                                  class="form-control">{{ old('recipe', $item->recipe) }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
