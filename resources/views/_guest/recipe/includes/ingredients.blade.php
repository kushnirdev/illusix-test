<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Ingredients</div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ingredient</th>
                            <th>Weight</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($recipeIngredients as $recipeIngredient)
                        @php /** @var \App\Models\RecipeIngredient $recipeIngredient */ @endphp
                        <tr>
                            <td>{{ $recipeIngredient->id }}</td>
                            <td>{{ $recipeIngredient->ingredients->name }}</td>
                            <td>{{ $recipeIngredient->weight }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>

@if($recipeIngredients->total() > $recipeIngredients->count())
    <br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{ $recipeIngredients->links() }}
                </div>
            </div>
        </div>
    </div>
@endif
