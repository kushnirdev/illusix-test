@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @php /** @var \App\Models\Recipe $item */ @endphp
            <div class="col-md-2">
                <div class="row justify-content-center">
{{--                    <img src="/images/{{ $item->image ?? 'default.png' }}" alt="company_logo" class="card-img figure-img" height="200">--}}
                </div>
            </div>
            <div class="col-md-8 ml-5">
                <div class="row justify-content-center">
                    <div class="mb-2">
                        {{ $item->name }}
                    </div>
                    {{ $item->description }}
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                @include('_guest.recipe.includes.ingredients')
            </div>
        </div>

    </div>
@endsection
