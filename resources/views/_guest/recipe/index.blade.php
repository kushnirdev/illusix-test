
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @foreach($items as $item)
            @php /** @var \App\Models\Recipe $item */ @endphp
                <div class="col-md-2 mt-3">
{{--                    <div class="row justify-content-center">--}}
{{--                        <a href="{{ route('guest.companies.show', $item->id) }}">--}}
{{--                                <img src="/images/{{ $item->image ? $item->image : 'default.png' }}" alt="company_logo" class="card-img figure-img" height="200">--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>
                <div class="col-md-8 ml-5">
                    <div class="row justify-content-center">
                                <div class="mb-2">
                                    <a href="{{ route('guest.recipes.show', $item->id) }}">{{ $item->name }}</a>
                                </div>
                                {{ $item->description }}
                        <div class="img-thumbnail bg-info ml-auto">Ingredients: {{ $item->recipe_ingredients_count }}</div>
                    </div>
                </div>
            @endforeach

            @if($items->total() > $items->count())
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                {{ $items->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
