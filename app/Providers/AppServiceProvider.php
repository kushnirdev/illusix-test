<?php

namespace App\Providers;

use App\Models\Ingredient;
use App\Models\Recipe;
use App\Models\RecipeIngredient;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Arr;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Check the same ingredients from the recipe when adding new ingredient
         *
         * uniqueUserIngredient:{recipe_id},{id}
         */
        Validator::extend('uniqueRecipeIngredient', function ($attribute, $value, $parameters, $validator) {
            $recipeId = Arr::get($validator->getData(), $parameters[0]);
            if (isset($parameters[1]))
            {
                $id = Arr::get($validator->getData(), $parameters[1]);
                $count = DB::table(app(RecipeIngredient::class)->getTable())
                    ->where($attribute, $value)
                    ->where($parameters[0], $recipeId)
                    ->whereNotIn($parameters[1], [$id])
                    ->count();
            } else {
                $count = DB::table(app(RecipeIngredient::class)->getTable())
                    ->where($attribute, $value)
                    ->where($parameters[0], $recipeId)
                    ->count();
            }

            return $count === 0;
        }, 'Current recipe already contains this ingredient!');

        /**
         * Check the same recipes from the user
         *
         * uniqueUserIngredient:{user_id},{id}
         */
        Validator::extend('uniqueUserRecipe', function ($attribute, $value, $parameters, $validator) {
            $userId = Arr::get($validator->getData(), $parameters[0]);
            if (isset($parameters[1]))
            {
                $id = Arr::get($validator->getData(), $parameters[1]);
                $count = DB::table(app(Recipe::class)->getTable())
                    ->where($attribute, $value)
                    ->where($parameters[0], $userId)
                    ->whereNotIn($parameters[1], [$id])
                    ->count();
            } else {
                $count = DB::table(app(Recipe::class)->getTable())
                    ->where($attribute, $value)
                    ->where($parameters[0], $userId)
                    ->count();
            }

            return $count === 0;
        }, 'Recipe with that name already exists in your recipe book! Please choose another name for this recipe!');

        /**
         * Check the same ingredients from the user
         *
         * uniqueUserIngredient:{user_id},{id}
         */
        Validator::extend('uniqueUserIngredient', function ($attribute, $value, $parameters, $validator) {
            $userId = Arr::get($validator->getData(), $parameters[0]);
            if (isset($parameters[1]))
            {
                $id = Arr::get($validator->getData(), $parameters[1]);
                $count = DB::table(app(Ingredient::class)->getTable())
                    ->where($attribute, $value)
                    ->where($parameters[0], $userId)
                    ->whereNotIn($parameters[1], [$id])
                    ->count();
            } else {
                $count = DB::table(app(Ingredient::class)->getTable())
                    ->where($attribute, $value)
                    ->where($parameters[0], $userId)
                    ->count();
            }

            return $count === 0;
        }, 'Ingredient with that name already exists in your ingredient library!');
    }
}
