<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|min:3|max:50|uniqueUserRecipe:user_id',
            'user_id'     => 'integer',
            'description' => 'nullable|string|max:100',
            'recipe'      => 'nullable|string|max:1000',
        ];
    }
}
