<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\IngredientCreateRequest;
use App\Http\Requests\IngredientUpdateRequest;
use App\Models\Ingredient;
use App\Models\RecipeIngredient;
use App\Repositories\IngredientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = auth()->user()->id;
        $items = app(IngredientRepository::class)->getMyWithPaginate($userId, 15);
        return view('_client.ingredient.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = app(Ingredient::class);
        $userId = auth()->user()->id;

        return view('_client.ingredient.create', compact('item', 'userId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  IngredientCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IngredientCreateRequest $request)
    {
        $data = $request->input();
        $item = (new Ingredient())->create($data);

        if ($item) {
            return redirect()
                ->route('client.ingredients.index')
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Saving was failed'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Ingredient::find($id);
        return view('_client.ingredient.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  IngredientUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IngredientUpdateRequest $request, $id)
    {
        $item = Ingredient::find($id);
        if (empty($item) || auth()->user()->id != $item->user_id) {
            return back()
                ->withErrors(['msg' => "Ingredient id={$id} not found"])
                ->withInput();
        }
        $data = $request->input();

        $result = $item->update($data);

        if ($result) {
            return redirect()
                ->route('client.ingredients.index')
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Saving was failed'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = false;
        $check = RecipeIngredient::where(['ingredient_id' => $id])->first();
        if (!$check) {
            $result = Ingredient::find($id)->delete();
        }

        if ($result) {
            return redirect()
                ->route('client.ingredients.index')
                ->with(['success' => 'Successfully deleted']);
        } else {
            return back()->withErrors(['msg' => 'Deleting was failed. Check this ingredient in yours recipes!']);
        }
    }
}
