<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\IngredientUpdateRequest;
use App\Http\Requests\RecipeCreateRequest;
use App\Http\Requests\RecipeUpdateRequest;
use App\Models\Recipe;
use App\Repositories\RecipeIngredientRepository;
use App\Repositories\RecipeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = auth()->user()->id;
        $items = app(RecipeRepository::class)->getMyWithPaginate($userId, 10);
        return view('_client.recipe.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = app(Recipe::class);
        $userId = auth()->user()->id;

        return view('_client.recipe.create', compact('item', 'userId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RecipeCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeCreateRequest $request)
    {
        $data = $request->input();
        $item = (new Recipe())->create($data);

        if ($item) {
            return redirect()
                ->route('client.recipes.show', $item->id)
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Saving was failed'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /**
         * @var Recipe $item
         */
        $item = Recipe::find($id);
        $recipeIngredients = app(RecipeIngredientRepository::class)->getAllWithPaginate($id, 20);
        return view('_client.recipe.show', compact('item','recipeIngredients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Recipe::find($id);
        return view('_client.recipe.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RecipeUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeUpdateRequest $request, $id)
    {
        $item = Recipe::find($id);
        if (empty($item) || auth()->user()->id != $item->user_id) {
            return back()
                ->withErrors(['msg' => "Recipe id={$id} not found"])
                ->withInput();
        }
        $data = $request->input();

        $result = $item->update($data);

        if ($result) {
            return redirect()
                ->route('client.recipes.index')
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Saving was failed'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Recipe::find($id)->delete();

        if ($result) {
            return redirect()
                ->route('client.recipes.index')
                ->with(['success' => 'Successfully deleted']);
        } else {
            return back()->withErrors(['msg' => 'Deleting was failed.']);
        }
    }
}
