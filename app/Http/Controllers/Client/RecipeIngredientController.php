<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\RecipeIngredientCreateRequest;
use App\Http\Requests\RecipeIngredientUpdateRequest;
use App\Models\Ingredient;
use App\Models\RecipeIngredient;
use App\Repositories\IngredientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecipeIngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function addingredient($id)
    {
        $userId = auth()->user()->id;
        $item = app(RecipeIngredient::class);
        $ingredientList = app(IngredientRepository::class)->getAllForSelect($userId);

        return view('_client.recipe-ingredient.create', compact('item', 'id', 'ingredientList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RecipeIngredientCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipeIngredientCreateRequest $request)
    {
        $data = $request->input();
        $item = (new RecipeIngredient())->create($data);

        if ($item) {
            return redirect()
                ->route('client.recipes.show', $item->recipe_id)
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Saving was failed'])
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = RecipeIngredient::find($id);
        $name = Ingredient::find($item->ingredient_id)->name;
        return view('_client.recipe-ingredient.edit', compact('item', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RecipeIngredientUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipeIngredientUpdateRequest $request, $id)
    {
        $item = RecipeIngredient::find($id);
        if (empty($item)) {
            return back()
                ->withErrors(['msg' => "Ingredient id={$id} not found"])
                ->withInput();
        }
        $data = $request->input();

        $result = $item->update($data);

        if ($result) {
            return redirect()
                ->route('client.recipes.show', $item->recipe_id)
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Saving was failed'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = RecipeIngredient::find($id);
        $result = $item->delete();

        if ($result) {
            return redirect()
                ->route('client.recipes.show', $item->recipe_id)
                ->with(['success' => 'Successfully deleted']);
        } else {
            return back()->withErrors(['msg' => 'Deleting was failed.']);
        }
    }
}
