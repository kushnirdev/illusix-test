<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ingredient
 * @package App\Models
 *
 * @property integer                 $id
 * @property integer                 $user_id
 * @property string                  $name
 * @property string                  $description
 * @property string                  $created_at
 * @property string                  $updated_at
 */
class Ingredient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id',
    ];
}
