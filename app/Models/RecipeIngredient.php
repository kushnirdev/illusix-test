<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Recipe
 * @package App\Models
 *
 * @property integer                 $id
 * @property integer                 $recipe_id
 * @property integer                 $ingredient_id
 * @property integer                 $weight
 */
class RecipeIngredient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recipe_id', 'ingredient_id', 'weight',
    ];

    /**
     * Ingredients
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ingredients()
    {
        return $this->hasOne(Ingredient::class, 'id', 'ingredient_id');
    }

    /**
     * Disable inserts created_at and updated_at fields
     *
     * @var bool $timestamps
     */
    public $timestamps = false;
}
