<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Recipe
 * @package App\Models
 *
 * @property integer                 $id
 * @property integer                 $user_id
 * @property string                  $name
 * @property string                  $recipe
 * @property string                  $description
 * @property string                  $created_at
 * @property string                  $updated_at
 */
class Recipe extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'recipe', 'description',
    ];

    /**
     * recipeIngredients
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipeIngredients()
    {
        return $this->hasMany(RecipeIngredient::class, 'recipe_id', 'id');
    }
}
