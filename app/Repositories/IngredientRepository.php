<?php
namespace App\Repositories;

use App\Models\Ingredient as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class IngredientRepository
 *
 * @package App\Repositories
 */

class IngredientRepository extends BaseRepository
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Get model for edit.
     *
     * @param int $id
     *
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * Get all Ingredient data
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = [
            'id',
            'name',
            'description',
        ];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->paginate($perPage);

        return $result;
    }

    /**
     * Get Ingredient data by owner
     * @param int $userId
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function getMyWithPaginate($userId, $perPage = null)
    {
        $columns = [
            'id',
            'name',
            'description',
        ];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->where('user_id', $userId)
            ->paginate($perPage);

        return $result;
    }

    /**
     * Get all Ingredient data
     */
    public function getAllForSelect($userId)
    {
        $columns = [
            'id',
            'name',
        ];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->where('user_id', $userId)
            ->toBase()
            ->get();

        return $result;
    }
}
