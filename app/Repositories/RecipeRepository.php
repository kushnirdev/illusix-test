<?php
namespace App\Repositories;

use App\Models\Recipe as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class RecipeRepository
 *
 * @package App\Repositories
 */

class RecipeRepository extends BaseRepository
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Get model for edit.
     *
     * @param int $id
     *
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * Get all Recipe data
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = [
            'id',
            'name',
            'recipe',
            'description',
        ];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->withCount('recipeIngredients')
            ->orderBy('recipe_ingredients_count', 'desc')
            ->paginate($perPage);

        return $result;
    }

    /**
     * Get Recipe data by owner
     * @param int $userId
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function getMyWithPaginate($userId, $perPage = null)
    {
        $columns = [
            'id',
            'name',
            'recipe',
            'description',
        ];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->where('user_id', $userId)
            ->withCount('recipeIngredients')
            ->orderBy('recipe_ingredients_count', 'desc')
            ->paginate($perPage);

        return $result;
    }
}
