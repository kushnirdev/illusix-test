<?php
namespace App\Repositories;

use App\Models\RecipeIngredient as Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class RecipeIngredientRepository
 *
 * @package App\Repositories
 */

class RecipeIngredientRepository extends BaseRepository
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Get model for edit.
     *
     * @param int $id
     *
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * Get all Ingredient data
     * @param int $id recipe id
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function getAllWithPaginate($id, $perPage = null)
    {
        $columns = [
            'id',
            'recipe_id',
            'ingredient_id',
            'weight',
        ];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->where('recipe_id', $id)
            ->with([
                'ingredients' => function ($query) {
                    $query->select(['id', 'name']);
                },
            ])
            ->paginate($perPage);

        return $result;
    }
}
