<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['web', 'auth']], function () {
    /**
     * Route group for clients
     */
    Route::group(['namespace' => 'Client', 'prefix' => 'client'], function() {
        Route::get('recipe-ingredients/{id}/addingredient', 'RecipeIngredientController@addingredient')->name('client.recipe-ingredient.addingredient');
        Route::resource('recipes', 'RecipeController')->names('client.recipes');
        Route::resource('ingredients', 'IngredientController')->names('client.ingredients');
        Route::resource('recipe-ingredients', 'RecipeIngredientController')->names('client.recipe-ingredients');
    });
});
