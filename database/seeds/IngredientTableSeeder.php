<?php

use Illuminate\Database\Seeder;

class IngredientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 21; $i++) {
            $data[] = [
                'name' => 'Ingredient ' . $i,
                'user_id' => 1,
                'description' => 'description',
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        DB::table('ingredients')->insert($data);
    }
}
