<?php

use Illuminate\Database\Seeder;

class RecipeIngredientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 21; $i++) {
            $r = rand(3, 6);
            $s = rand(1, 8);
            for ($j = 0; $j < $r; $j++) {
                $data[] = [
                    'recipe_id' => $i,
                    'ingredient_id' => $s,
                    'weight' => 500,
                ];
                $s +=2;
            }
        }
        DB::table('recipe_ingredients')->insert($data);
    }
}
