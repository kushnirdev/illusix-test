<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'     => 'Admin',
                'email'    => 'admin@gmail.com',
                'password' => Hash::make('123123123'),
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
            [
                'name'     => 'User1',
                'email'    => 'user@gmail.com',
                'password' => Hash::make('123123123'),
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
            ],
        ];
        DB::table('users')->insert($data);
    }
}
