<?php

use Illuminate\Database\Seeder;

class RecipeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 21; $i++) {
            $data[] = [
                'name' => 'Recipe ' . $i,
                'user_id' => 1,
                'recipe' => 'recipe',
                'description' => 'description',
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        DB::table('recipes')->insert($data);
    }
}
